# Command and Data characteristics
CMD_UUID = "d5913036-2d8a-41ee-85b9-4e361aa5c8a7"
DATA_UUID = "09bf2c52-d1d9-c0b7-4145-475964544307"


from bleak import BleakScanner, BleakClient
import asyncio

def command_characteristic_callback(sender, data):
    print(f"response using callback function: {data}")

def data_characteristic_callback(sender,data):
    print(f"data: {data}")

async def main():
    # Device name to be searched for
    my_device_name = 'muse_v3'

    # Device Enumeration
    devices = await BleakScanner.discover()
    myDevice = None
    for d in devices:
        print(d)
        if d.name == my_device_name:
            myDevice = d

    if(myDevice != None):
        # Device Connection
        async with BleakClient(str(myDevice.address)) as client:
            print('List of all characteristics: ')
            for characteristic in client.services.characteristics:
                x = client.services.characteristics.get(characteristic)
                print(client.services.get_service(x.service_handle).description, x.service_uuid, x.description, x.uuid, x.properties)


            # Send the GET STATE command
            await client.write_gatt_char(char_specifier=CMD_UUID, data=bytearray([0x82,0x00]), response=True)
            # Read the response from the characteristic and print to the console
            response = await client.read_gatt_char(CMD_UUID)
            print(f"command response: {response}")

            # Subscribe to command characteristic
            await client.start_notify(CMD_UUID, command_characteristic_callback)
            await client.write_gatt_char(char_specifier=CMD_UUID, data=bytearray([0x82,0x00]), response=True)

            # Subscribe to Data characteristic
            await client.start_notify(DATA_UUID, data_characteristic_callback)
            # Start buffered streaming of accelerometer data and read data
            await client.write_gatt_char(char_specifier=CMD_UUID, data=bytearray([0x02,0x05,0x06,0x02,0x00,0x00,0x01]), response=True)
            # Make the streaming last for 5 seconds
            await asyncio.sleep(5)
            # Stop the streaming
            await client.write_gatt_char(char_specifier=CMD_UUID, data=bytearray([0x02,0x01,0x02]), response=True)

            await client.stop_notify(CMD_UUID)
            await client.stop_notify(DATA_UUID)

asyncio.run(main())